package com.cm.cpickl.quarkusplayground.borealis.client.ktor

import com.cm.cpickl.quarkusplayground.borealis.apimodels.BorealisQueryParam
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.parameter

fun HttpRequestBuilder.parameter(pageRequest: PageRequest) {
    parameter(BorealisQueryParam.SKIP, pageRequest.skip)
    parameter(BorealisQueryParam.TAKE, pageRequest.take)
}
