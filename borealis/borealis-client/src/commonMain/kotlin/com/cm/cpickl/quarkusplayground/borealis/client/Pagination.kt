package com.cm.cpickl.quarkusplayground.borealis.client

import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest

fun PageRequest.toQueryParams() = "skip=$skip&take=$take"

fun PageRequest.toQueryTuples() = listOf("skip" to skip.toString(), "take" to take.toString())
