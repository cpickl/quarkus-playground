plugins {
    kotlin("multiplatform")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":borealis:borealis-api-models"))
            }
        }
    }
}
