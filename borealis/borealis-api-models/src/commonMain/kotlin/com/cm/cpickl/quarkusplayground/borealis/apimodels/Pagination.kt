package com.cm.cpickl.quarkusplayground.borealis.apimodels

import com.cm.cpickl.quarkusplayground.commons.apimodels.Dto
import kotlinx.serialization.Serializable

data class PageRequest(
    val skip: Int,
    val take: Int,
) {
    companion object {
        const val defaultTake = 10
        const val defaultSkip = 0

        val default = PageRequest(
            skip = defaultSkip,
            take = defaultTake,
        )
    }
}

@Serializable
data class PagedResponseDto<T>(
    val items: List<T>,
    val totalItems: Long,
) : Dto {
    fun <R> map(transformer: (T) -> R): PagedResponseDto<R> =
        PagedResponseDto(
            items = items.map(transformer),
            totalItems = totalItems,
        )
}
