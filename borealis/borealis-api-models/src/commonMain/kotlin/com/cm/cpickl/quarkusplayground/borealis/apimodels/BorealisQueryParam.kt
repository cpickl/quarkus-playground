package com.cm.cpickl.quarkusplayground.borealis.apimodels

object BorealisQueryParam {
    const val SKIP = "skip"
    const val TAKE = "take"
}
