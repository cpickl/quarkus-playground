package com.cm.cpickl.quarkusplayground.borealis.server.quarkus

import com.cm.cpickl.quarkusplayground.borealis.apimodels.BorealisQueryParam
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import javax.ws.rs.DefaultValue
import javax.ws.rs.QueryParam

data class PageRequestBean(

    @field:QueryParam(BorealisQueryParam.SKIP)
    @field:DefaultValue("${PageRequest.defaultSkip}")
    var skip: Int = PageRequest.defaultSkip,

    @field:QueryParam(BorealisQueryParam.TAKE)
    @field:DefaultValue("${PageRequest.defaultTake}")
    var take: Int = PageRequest.defaultTake,

    ) {
    companion object {
        val default = PageRequest.default.toPageRequestBean()
    }

    fun toPageRequest() = PageRequest(
        skip = skip,
        take = take,
    )
}

fun PageRequest.toPageRequestBean() = PageRequestBean(
    skip = skip,
    take = take,
)
