plugins {
    kotlin("jvm")
}
dependencies {
    api(project(":borealis:borealis-api-models"))
    implementation("org.jboss.spec.javax.ws.rs:jboss-jaxrs-api_2.1_spec:2.0.1.Final")
}
