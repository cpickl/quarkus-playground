package com.cm.cpickl.quarkusplayground.commons.domainmodels

import arrow.core.Either
import arrow.core.continuations.either
import arrow.core.left

data class Money(
    val currency: Currency,
    val amount: Int,
) {
    companion object
}

data class Currency(
    val code: String,
    val precision: Int,
) {
    companion object {
        suspend fun byCurrencyCode(code: String): Either<CurrencyNotFoundFail, Currency> = either {
            val currency = currenciesByCode[code]
            if (currency == null) {
                CurrencyNotFoundFail(code).left()
            }
            currency!!
        }

        val euro = Currency("EUR", 2)

        private val currenciesByCode = listOf(euro).associateBy { it.code }
    }

    init {
        require(code.length == 3)
        require(precision >= 0)
    }
}

class CurrencyNotFoundFail(code: String) {
    val message = "Currency code not found: [$code]"
}
