plugins {
    kotlin("jvm")
}

dependencies {
    implementation("io.arrow-kt:arrow-core:1.1.2")
}
