plugins {
    kotlin("multiplatform")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":commons:common-api-models"))
                implementation("io.kotest:kotest-property:5.5.1")
            }
        }
    }
}
