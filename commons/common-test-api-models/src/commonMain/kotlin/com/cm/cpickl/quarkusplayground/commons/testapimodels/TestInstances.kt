package com.cm.cpickl.quarkusplayground.commons.testapimodels

import com.cm.cpickl.quarkusplayground.commons.apimodels.CurrencyDto
import com.cm.cpickl.quarkusplayground.commons.apimodels.MoneyDto
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string

fun Arb.Companion.moneyDto() = arbitrary {
    MoneyDto(
        currency = currencyDto().next(),
        amount = int(min = 0).next()
    )
}

fun Arb.Companion.currencyDto() = arbitrary {
    CurrencyDto(
        code = string(minSize = 3, maxSize = 3, codepoints = Codepoint.alphanumeric()).next(),
        precision = int(min = 0).next()
    )
}
