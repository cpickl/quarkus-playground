plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser {
            testTask {
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation("io.kotest:kotest-framework-engine:5.5.1")
                implementation("io.kotest:kotest-framework-api:5.5.1")
                implementation("io.kotest:kotest-assertions-core:5.5.1")
                implementation("io.kotest:kotest-assertions-json:5.5.1")
                implementation("io.kotest:kotest-property:5.5.1")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("io.kotest:kotest-runner-junit5:5.5.1")
            }
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
