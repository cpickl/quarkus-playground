package com.cm.cpickl.quarkusplayground.commons.apimodels

import io.kotest.assertions.json.shouldEqualJson
import io.kotest.core.spec.style.StringSpec
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class MoneyTest : StringSpec({
    "When serialize euro money Then return JSON" {
        val money = MoneyDto(CurrencyDto.euro, 42)

        Json.encodeToString(money) shouldEqualJson """{ "amount": 42, "currency": { "code": "EUR", "precision": 2 } }"""
    }
})
