package com.cm.cpickl.quarkusplayground.commons.apimodels

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class UuidTest : StringSpec({
    "When serialize euro money Then return JSON" {
        val uuid = Uuid.random()

        Json.encodeToString(uuid) shouldBe "\"${uuid.value}\""
    }
})
