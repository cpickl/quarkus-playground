package com.cm.cpickl.quarkusplayground.commons.apimodels

import kotlinx.serialization.Serializable
import java.util.UUID

@JvmInline
@Serializable
actual value class Uuid private constructor(val value: String) {
    actual companion object {
        fun fromUUID(uuid: UUID) =
            Uuid(uuid.toString())

        actual fun random(): Uuid =
            fromUUID(UUID.randomUUID())

        actual operator fun invoke(value: String): Uuid {
            require(uuidRegex.matches(value)) { "Passed a non-valid UUID: '$value'!" }
            return Uuid(UUID.fromString(value.lowercase()).toString())
        }
    }

    fun toUUID(): UUID = UUID.fromString(value)
}
