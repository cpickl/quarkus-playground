package com.cm.cpickl.quarkusplayground.commons.apimodels

import kotlinx.serialization.Serializable

@Serializable
actual value class Uuid private constructor(val value: String) {
    actual companion object {

        private val chars = "abcdef0123456789".toCharArray()

        actual fun random(): Uuid =
            Uuid(listOf(8, 4, 4, 4, 12).joinToString("-") { length ->
                (1..length).map { chars.random() }.joinToString("")
            })

        actual operator fun invoke(value: String): Uuid {
            require(uuidRegex.matches(value)) { "Passed a non-valid UUID: '$value'!" }
            return Uuid(value.lowercase())
        }
    }
}
