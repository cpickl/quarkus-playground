package com.cm.cpickl.quarkusplayground.commons.apimodels

import kotlinx.serialization.Serializable

@Serializable
data class MoneyDto(
    val currency: CurrencyDto,
    val amount: Int,
)

@Serializable
data class CurrencyDto(
    val code: String,
    val precision: Int,
) {
    companion object {
        val euro = CurrencyDto("EUR", 2)
    }

    init {
        require(code.length == 3)
        require(precision >= 0)
    }
}
