package com.cm.cpickl.quarkusplayground.commons.apimodels

import kotlinx.serialization.Serializable

internal val uuidRegex =
    Regex("""^[\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}${'$'}""")

@Serializable
expect value class Uuid private constructor(val value: String) {
    companion object {
        fun random(): Uuid
        operator fun invoke(value: String): Uuid
    }
}
