package com.cm.cpickl.quarkusplayground.commons.ktor.mock

import com.cm.cpickl.quarkusplayground.commons.ktor.core.toHeaders
import com.cm.cpickl.quarkusplayground.commons.ktor.core.toListOfPairs
import io.kotest.matchers.shouldBe
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.ByteReadChannel

// TODO make it a DSL
data class MockStub(
    val expectedPath: String,
    val expectedMethod: HttpMethod = HttpMethod.Get,
    val expectedQueryParameters: List<Pair<String, String>> = emptyList(),
    // expectedHeaders
    val responseBody: String? = null,
    val responseStatus: HttpStatusCode = HttpStatusCode.OK,
    val responseHeaders: Map<String, String>? = null,
)

fun buildMockHttpClient(stub: MockStub) = MockEngine { request ->
    request.url.encodedPath shouldBe stub.expectedPath
    request.url.parameters.toListOfPairs() shouldBe stub.expectedQueryParameters
    request.method shouldBe stub.expectedMethod

    val responseHeadersMap = mutableMapOf<String, String>().apply {
        if (stub.responseHeaders != null) {
            putAll(stub.responseHeaders)
        }
    }

    respond(
        content = stub.responseBody?.let { ByteReadChannel(it) } ?: ByteReadChannel.Empty,
        status = stub.responseStatus,
        headers = responseHeadersMap.toHeaders(),
    )
}
