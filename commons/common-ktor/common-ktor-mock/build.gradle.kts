plugins {
    kotlin("multiplatform")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":commons:common-ktor:common-ktor-core"))
                api("io.ktor:ktor-client-mock:2.1.1")
                implementation("io.kotest:kotest-framework-engine:5.5.1")
                implementation("io.kotest:kotest-framework-api:5.5.1")
                implementation("io.kotest:kotest-assertions-core:5.5.1")
                implementation("io.kotest:kotest-property:5.5.1")
            }
        }
    }
}
