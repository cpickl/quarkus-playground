package com.cm.cpickl.quarkusplayground.commons.ktor.core

import com.cm.cpickl.quarkusplayground.commons.lang.toListOfPairs
import io.ktor.http.Parameters
import io.ktor.http.headersOf
import io.ktor.util.toMap

fun Map<String, String>.toHeaders() =
    headersOf(*toMultiValues().toList().toTypedArray())

fun Map<String, String>.toMultiValues(): Map<String, List<String>> =
    map { it.key to listOf(it.value) }.toMap()

fun Parameters.toListOfPairs(): List<Pair<String, String>> =
    toMap().toListOfPairs()
