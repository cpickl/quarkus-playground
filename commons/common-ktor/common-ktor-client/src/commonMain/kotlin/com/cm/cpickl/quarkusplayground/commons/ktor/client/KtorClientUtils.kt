package com.cm.cpickl.quarkusplayground.commons.ktor.client

import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode

fun HttpRequestBuilder.accept(mediaType: ContentType) {
    header("Accept", mediaType.toString())
}

fun HttpRequestBuilder.acceptJson() {
    accept(ContentType.Application.Json)
}

fun HttpResponse.requireOk() = apply {
    if (status != HttpStatusCode.OK) {
        error("Expected OK but got response: $status")
    }
}
