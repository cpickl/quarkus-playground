plugins {
    kotlin("multiplatform")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
}
