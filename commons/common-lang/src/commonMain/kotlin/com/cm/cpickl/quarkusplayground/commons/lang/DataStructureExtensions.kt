package com.cm.cpickl.quarkusplayground.commons.lang

fun <K, V> Map<K, List<V>>.toListOfPairs(): List<Pair<K, V>> =
    flatMap { entry -> entry.value.map { value -> entry.key to value } }
