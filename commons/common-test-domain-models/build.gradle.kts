plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":commons:common-domain-models"))
    implementation("io.kotest:kotest-property:5.5.1")
}
