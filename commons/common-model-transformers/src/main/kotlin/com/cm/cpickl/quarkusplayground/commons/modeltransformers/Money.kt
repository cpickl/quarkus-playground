package com.cm.cpickl.quarkusplayground.commons.modeltransformers

import com.cm.cpickl.quarkusplayground.commons.apimodels.CurrencyDto
import com.cm.cpickl.quarkusplayground.commons.apimodels.MoneyDto
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Currency
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Money

fun MoneyDto.toMoney() = Money(
    currency = currency.toCurrency(),
    amount = amount
)

fun CurrencyDto.toCurrency() = Currency(
    code = code,
    precision = precision
)

fun Money.toMoneyDto() = MoneyDto(
    currency = currency.toCurrencyDto(),
    amount = amount
)

fun Currency.toCurrencyDto() = CurrencyDto(
    code = code,
    precision = precision
)
