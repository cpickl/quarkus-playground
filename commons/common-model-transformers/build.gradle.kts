plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":commons:common-api-models"))
    implementation(project(":commons:common-domain-models"))
}
