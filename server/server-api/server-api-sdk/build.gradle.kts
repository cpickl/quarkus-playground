plugins {
    id("io.kotest.multiplatform")
    kotlin("plugin.serialization")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser {
            testTask {
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":server:server-api:server-api-models"))
                api(project(":borealis:borealis-api-models"))
                api(project(":borealis:borealis-client:borealis-client-ktor"))
                implementation(project(":commons:common-ktor:common-ktor-client"))
                implementation("io.ktor:ktor-client-core:2.1.3")
                implementation("io.ktor:ktor-client-logging:2.1.3")
                implementation("io.ktor:ktor-client-content-negotiation:2.1.3")
                implementation("io.ktor:ktor-serialization-kotlinx-json:2.1.3")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
                implementation("io.github.microutils:kotlin-logging:2.1.23")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(project(":commons:common-ktor:common-ktor-mock"))
                implementation(project(":server:server-api:server-api-test-models"))
                implementation("io.kotest:kotest-framework-engine:5.5.1")
                implementation("io.kotest:kotest-framework-api:5.5.1")
                implementation("io.kotest:kotest-assertions-core:5.5.1")
                implementation("io.kotest:kotest-property:5.5.1")
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:2.1.3")
            }
        }
        val jsMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-js:2.1.3")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("io.kotest:kotest-runner-junit5:5.5.1")
            }
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
