package com.cm.cpickl.quarkusplayground.server.api.sdk

import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest

interface ServerApi {
    suspend fun homeGreetingString(): String
    suspend fun findAllTransactions(pageRequest: PageRequest): TransactionsSummaryResponseDto

    // TODO to TransactionId type
    suspend fun findTransaction(id: String): TransactionsDetailDto?
}
