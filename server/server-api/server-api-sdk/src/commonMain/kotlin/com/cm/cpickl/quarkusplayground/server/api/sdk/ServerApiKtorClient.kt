package com.cm.cpickl.quarkusplayground.server.api.sdk

import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.client.ktor.parameter
import com.cm.cpickl.quarkusplayground.commons.ktor.client.accept
import com.cm.cpickl.quarkusplayground.commons.ktor.client.acceptJson
import com.cm.cpickl.quarkusplayground.commons.ktor.client.requireOk
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import mu.KotlinLogging.logger

// TODO write tests in server to verify implementation to contract!
class ServerApiKtorClient(
    serverBaseUrl: String,
    /** If null, will autodetect JVM's CIO or JS. */
    engine: HttpClientEngine? = null
) : ServerApi {

    private val log = logger {}

    private val httpClient = buildHttpClient(serverBaseUrl, engine)

    override suspend fun homeGreetingString(): String {
        log.info { "homeGreetingString()" }
        return httpClient.get("/") {
            accept(ContentType.Text.Plain)
        }.requireOk().bodyAsText()
    }

    override suspend fun findAllTransactions(pageRequest: PageRequest): TransactionsSummaryResponseDto {
        log.info { "findAllTransactions($pageRequest)" }
        return httpClient.get("/transactions") {
            acceptJson()
            parameter(pageRequest)
        }.requireOk().body()
    }

    override suspend fun findTransaction(id: String): TransactionsDetailDto? {
        log.info { "findTransaction($id)" }
        val response = httpClient.get("/transactions/$id") {
            acceptJson()
        }
        return when (response.status) {
            HttpStatusCode.OK -> response.body()
            HttpStatusCode.NotFound -> null
            else -> error("Unexpected response status code: ${response.status}")
        }
    }
}

private fun buildHttpClient(serverBaseUrl: String, engine: HttpClientEngine?) =
    engine?.let { HttpClient(it) } ?: HttpClient() {
        defaultRequest {
            url(serverBaseUrl)
            headers.append("Agent", "server-api-ktor-client")
        }
        install(Logging) {
            level = LogLevel.ALL
        }
        install(ContentNegotiation) {
            json(
                Json {
                    encodeDefaults = true
                    isLenient = false
                    allowSpecialFloatingPointValues = true
                    allowStructuredMapKeys = true
                    prettyPrint = true
                    useArrayPolymorphism = false
                })
        }
        expectSuccess = false
    }