package com.cm.cpickl.quarkusplayground.server.api.sdk

import com.cm.cpickl.quarkusplayground.api.test.pagedResponseOfTransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.client.toQueryTuples
import com.cm.cpickl.quarkusplayground.commons.ktor.mock.MockStub
import com.cm.cpickl.quarkusplayground.commons.ktor.mock.buildMockHttpClient
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ServerApiKtorClientTest : StringSpec() {

    private val transactionsPage = Arb.pagedResponseOfTransactionsSummaryDto().next()
    private val anyUrl = "http://any"
    private val pageRequest = PageRequest.default

    init {
        // TODO test failing: No transformation found: class io.ktor.utils.io.ByteBufferChannel -> class com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
        "Given server response When find all transactions Then return".config(enabled = false) {
            val client = buildClient(
                MockStub(
                    expectedPath = "/transactions",
                    expectedQueryParameters = pageRequest.toQueryTuples(),
                    responseBody = Json.encodeToString(transactionsPage),
                    responseHeaders = mapOf(HttpHeaders.ContentType to ContentType.Application.Json.toString()),
                )
            )

            val response = client.findAllTransactions(pageRequest)

            response shouldBe transactionsPage
        }
    }

    private fun buildClient(stub: MockStub) = ServerApiKtorClient(
        anyUrl, buildMockHttpClient(stub)
    )
}
