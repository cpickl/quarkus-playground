package com.cm.cpickl.quarkusplayground.api.test

import com.cm.cpickl.quarkusplayground.api.models.HealthResponseDto
import com.cm.cpickl.quarkusplayground.api.models.HomeResponseDto
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string

fun Arb.Companion.homeResponseDto() = arbitrary {
    HomeResponseDto(
        linkThis = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        linkTransactions = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        health = Arb.healthResponseDto().next(),
        linkOpenApi = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        linkSwaggerUi = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
    )
}

fun Arb.Companion.healthResponseDto() = arbitrary {
    HealthResponseDto(
        home = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        started = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        ready = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
        live = Arb.string(codepoints = Codepoint.alphanumeric()).next(),
    )
}
