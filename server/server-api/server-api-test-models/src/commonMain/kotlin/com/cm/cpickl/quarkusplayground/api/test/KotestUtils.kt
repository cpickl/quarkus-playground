package com.cm.cpickl.quarkusplayground.api.test

import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.string

fun link() = Arb.string(codepoints = Codepoint.alphanumeric())
