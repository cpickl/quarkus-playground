package com.cm.cpickl.quarkusplayground.api.test

import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.commons.testapimodels.moneyDto
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string
import io.kotest.property.arbitrary.take

fun Arb.Companion.transactionsSummaryResponseDto() = arbitrary {
    TransactionsSummaryResponseDto(
        page = PagedResponseDto(
            // TODO is there a nicer way to generate arbitrary lists?!
            items = transactionsSummaryDto().take(int(min = 0, max = 5).next()).toList(),
            totalItems = long(min = 10, max = 100).next(),
        )
    )
}

fun Arb.Companion.transactionsSummaryDto() = arbitrary {
    TransactionsSummaryDto(
        id = string(minSize = 1, codepoints = Codepoint.alphanumeric()).next(),
        money = moneyDto().next(),
        linkDetail = link().next(),
    )
}

fun Arb.Companion.transactionsDetailDto() = arbitrary {
    TransactionsDetailDto(
        id = string(minSize = 1, codepoints = Codepoint.alphanumeric()).next(),
        money = Arb.moneyDto().next(),
    )
}


