plugins {
    id("io.kotest.multiplatform") version "5.5.4"
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":server:server-api:server-api-models"))
                api(project(":commons:common-test-api-models"))
                api("io.kotest:kotest-property:5.5.1")
//                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation("io.kotest:kotest-framework-engine:5.5.1")
                implementation("io.kotest:kotest-framework-api:5.5.1")
                implementation("io.kotest:kotest-assertions-core:5.5.1")
                implementation("io.kotest:kotest-assertions-json:5.5.1")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
            }
        }
        // FIXME build error when publishing this artifact with coroutines not found! same for commons-api-test-models
        val jsMain by getting {
            dependencies {
//                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.6.4")
            }
        }
        val jvmMain by getting {
            dependencies {
//                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:1.6.4")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("io.kotest:kotest-runner-junit5:5.5.1")
            }
        }
    }
}
