package com.cm.cpickl.quarkusplayground.api.models

import com.cm.cpickl.quarkusplayground.commons.apimodels.Dto
import kotlinx.serialization.Serializable

@Serializable
data class HomeResponseDto(
    // TODO introduce custom MPP URL type + serializer
    val linkThis: String,
    val linkTransactions: String,
    val linkOpenApi: String,
    val linkSwaggerUi: String,
    val health: HealthResponseDto,
) : Dto

@Serializable
data class HealthResponseDto(
    val home: String,
    val started: String,
    val ready: String,
    val live: String,
) : Dto
