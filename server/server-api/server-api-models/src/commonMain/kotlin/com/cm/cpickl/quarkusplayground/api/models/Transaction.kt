package com.cm.cpickl.quarkusplayground.api.models

import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.commons.apimodels.Dto
import com.cm.cpickl.quarkusplayground.commons.apimodels.MoneyDto
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

// TODO can't make it a value class; serialization issue
//  ... kotlinx.serialization.SerializationException: Serializer for class 'TransactionsSummaryResponseDto' is not found.
// TODO now having this custom serializer, it doesn't match with the OpenAPI spec :-/
@Serializable(with = FlatteningTransactionsSummaryResponseDtoSerializer::class)
data class TransactionsSummaryResponseDto(
    val page: PagedResponseDto<TransactionsSummaryDto>
) : Dto

@Serializable
data class TransactionsSummaryDto(
    // TODO switch to type safe TransactionId (plus add serializer)
//   not possible, as OpenAPI annotations are not MPP ready ... @Schema(example = "32126319")
    val id: String,
    val money: MoneyDto,
    val linkDetail: String,
) : Dto

@Serializable
data class TransactionsDetailDto(
    val id: String,
    val money: MoneyDto,
) : Dto

object FlatteningTransactionsSummaryResponseDtoSerializer : KSerializer<TransactionsSummaryResponseDto> {

    private val delegate = PagedResponseDto.serializer(TransactionsSummaryDto.serializer())
    override val descriptor = buildClassSerialDescriptor("TransactionsSummaryResponseDto")

    override fun deserialize(decoder: Decoder): TransactionsSummaryResponseDto =
        TransactionsSummaryResponseDto(page = decoder.decodeSerializableValue(delegate))

    override fun serialize(encoder: Encoder, value: TransactionsSummaryResponseDto) {
        encoder.encodeSerializableValue(delegate, value.page)
    }
}
