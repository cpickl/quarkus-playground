package com.cm.cpickl.quarkusplayground.api.models

import com.cm.cpickl.quarkusplayground.commons.apimodels.Dto
import kotlinx.serialization.Serializable

@Serializable
data class ApiErrorDto(
    val message: String
) : Dto
