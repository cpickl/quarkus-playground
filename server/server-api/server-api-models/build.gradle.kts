plugins {
    kotlin("plugin.serialization")
}

kotlin {
    jvm() {
        withJava()
    }
    js(IR) {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":borealis:borealis-api-models"))
                api(project(":commons:common-api-models"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
                implementation("org.eclipse.microprofile.openapi:microprofile-openapi-api:2.0.1")
            }
        }
    }
}
