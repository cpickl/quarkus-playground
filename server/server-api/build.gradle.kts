allprojects {
    group = "com.cm.cpickl.quarkusplayground.server.api"
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.multiplatform")
}
