package com.cm.cpickl.quarkusplayground.server.presentation

import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.api.test.transactionsSummaryDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import io.kotest.assertions.json.shouldEqualJson
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test

class TransactionsApiModelsSerializationTest {

    @Test
    fun `When serialize TransactionsSummaryResponseDto Then return JSON`() {
        val transactionSummary = Arb.transactionsSummaryDto().next()
        val transactionsResponse = TransactionsSummaryResponseDto(
            page = PagedResponseDto(
                items = listOf(transactionSummary),
                totalItems = 1
            )
        )
        val json = Json.encodeToString(transactionsResponse)

        json shouldEqualJson """{
                "totalItems": ${transactionsResponse.page.totalItems},
                "items": [
                    {
                      "id": "${transactionSummary.id}",
                      "money": {
                        "amount": ${transactionSummary.money.amount},
                        "currency": {
                            "code": "${transactionSummary.money.currency.code}",
                            "precision": ${transactionSummary.money.currency.precision}
                        }
                      },
                      "linkDetail": "${transactionSummary.linkDetail}"
                    }
                ]
            }"""
    }
}
