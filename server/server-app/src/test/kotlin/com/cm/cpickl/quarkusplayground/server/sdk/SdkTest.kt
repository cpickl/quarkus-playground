package com.cm.cpickl.quarkusplayground.server.sdk

import arrow.core.left
import arrow.core.right
import com.cm.cpickl.quarkusplayground.api.test.transactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.test.transactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.server.quarkus.PageRequestBean
import com.cm.cpickl.quarkusplayground.server.api.sdk.ServerApi
import com.cm.cpickl.quarkusplayground.server.api.sdk.ServerApiKtorClient
import com.cm.cpickl.quarkusplayground.server.domain.logic.notFoundFail
import com.cm.cpickl.quarkusplayground.server.domain.model.transactionId
import com.cm.cpickl.quarkusplayground.server.presentation.controller.HomeController
import com.cm.cpickl.quarkusplayground.server.presentation.controller.TransactionController
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.mockk.coEvery
import io.mockk.every
import io.quarkiverse.test.junit.mockk.InjectMock
import io.quarkus.test.junit.QuarkusTest
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.junit.jupiter.api.Test

@QuarkusTest
class SdkTest {

    @Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
    @ConfigProperty(name = "quarkus.http.test-port")
    private lateinit var quarkusPort: Integer

    @InjectMock
    private lateinit var homeController: HomeController

    @InjectMock
    private lateinit var transactionsController: TransactionController

    private val pageRequestBean = PageRequestBean.default
    private val transaction = Arb.transactionsDetailDto().next()
    private val transactionsSummary = Arb.transactionsSummaryResponseDto().next()
    private val notFoundFail = Arb.notFoundFail().next()
    private val transactionId = Arb.transactionId().next()
    private val greetingText = "test greeting"

    @Test
    fun `Given controller returns string When get home greeting Then return that string`() = runUnitBlocking {
        every { homeController.greeting() } returns greetingText

        serverApi().homeGreetingString() shouldBe greetingText
    }

    @Test
    fun `Given controller returns When get paged transactions Then return that response`() = runUnitBlocking {
        coEvery { transactionsController.findAll(pageRequestBean) } returns transactionsSummary.right()

        serverApi().findAllTransactions(pageRequestBean.toPageRequest()) shouldBe transactionsSummary
    }

    @Test
    fun `Given controller returns When get transaction Then return that transaction`() = runUnitBlocking {
        coEvery { transactionsController.findSingle(transaction.id) } returns transaction.right()

        serverApi().findTransaction(transaction.id) shouldBe transaction
    }

    @Test
    fun `Given controller fails When get transaction Then return null`() = runUnitBlocking {
        coEvery { transactionsController.findSingle(transactionId.value) } returns notFoundFail.left()

        serverApi().findTransaction(transactionId.value).shouldBeNull()
    }

    private fun serverApi(): ServerApi = ServerApiKtorClient("http://localhost:$quarkusPort")
}
