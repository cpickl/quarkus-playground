package com.cm.cpickl.quarkusplayground.server.commons

import com.cm.cpickl.quarkusplayground.borealis.apimodels.BorealisQueryParam
import com.cm.cpickl.quarkusplayground.borealis.server.quarkus.PageRequestBean
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.parameter

fun HttpRequestBuilder.parameter(pageRequest: PageRequestBean) {
    parameter(BorealisQueryParam.SKIP, pageRequest.skip)
    parameter(BorealisQueryParam.TAKE, pageRequest.take)
}
