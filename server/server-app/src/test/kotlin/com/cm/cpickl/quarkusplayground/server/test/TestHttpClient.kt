package com.cm.cpickl.quarkusplayground.server.test

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.URLProtocol
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import mu.KotlinLogging.logger
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class TestHttpClient(
    explicitBaseUrl: String? = null,
) {
    @ConfigProperty(name = "quarkus.http.test-port")
    private lateinit var quarkusTestPort: Integer

    private val log = logger {}

    private val httpClient = HttpClient(CIO) {
        defaultRequest {
            if (explicitBaseUrl == null) {
                url {
                    protocol = URLProtocol.HTTP
                    host = "localhost"
                    port = quarkusTestPort.toInt()
                }
            } else {
                url(explicitBaseUrl)
            }
        }
        install(ContentNegotiation) {
            json(
                Json {
                    encodeDefaults = true
                    isLenient = false
                    allowSpecialFloatingPointValues = true
                    allowStructuredMapKeys = true
                    prettyPrint = true
                    useArrayPolymorphism = false
                })
        }
        expectSuccess = false
    }

    suspend fun get(path: String, requestBuilder: HttpRequestBuilder.() -> Unit = {}): HttpResponse {
        log.debug { "GET $path" }
        return httpClient.get(path, requestBuilder)
    }
}
