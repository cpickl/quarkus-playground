package com.cm.cpickl.quarkusplayground.server.adapter.persistence

import com.cm.cpickl.quarkusplayground.commons.domainmodels.Currency
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string

fun Arb.Companion.transactionDbo() = arbitrary {
    TransactionDbo(
        transactionId = string(minSize = 1, maxSize = 20, codepoints = Codepoint.alphanumeric()).next(),
        currencyCode = Currency.euro.code,
        amount = int(min = 0).next(),
    )
}
