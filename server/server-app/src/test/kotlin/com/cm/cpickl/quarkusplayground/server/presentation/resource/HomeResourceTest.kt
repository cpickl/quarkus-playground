package com.cm.cpickl.quarkusplayground.server.presentation.resource

import com.cm.cpickl.quarkusplayground.api.models.HomeResponseDto
import com.cm.cpickl.quarkusplayground.api.test.homeResponseDto
import com.cm.cpickl.quarkusplayground.commons.ktor.client.accept
import com.cm.cpickl.quarkusplayground.commons.ktor.client.acceptJson
import com.cm.cpickl.quarkusplayground.server.presentation.controller.HomeController
import com.cm.cpickl.quarkusplayground.server.test.TestHttpClient
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string
import io.ktor.client.call.body
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.mockk.every
import io.quarkiverse.test.junit.mockk.InjectMock
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
class HomeResourceTest {

    @Inject
    private lateinit var httpClient: TestHttpClient

    @InjectMock
    private lateinit var controller: HomeController

    private val homeText = Arb.string().next()
    private val homeResponseDto = Arb.homeResponseDto().next()

    @Test
    fun `Given controller returns greeting When get home for text Then return OK and body`() = runUnitBlocking {
        every { controller.greeting() } returns homeText

        val response = httpClient.get("/") {
            accept(ContentType.Text.Plain)
        }

        response.status shouldBe HttpStatusCode.OK
        response.bodyAsText() shouldBe homeText
    }

    @Test
    fun `Given controller returns response When get home for json Then return OK and body`() = runUnitBlocking {
        every { controller.home() } returns homeResponseDto

        val response = httpClient.get("/") {
            acceptJson()
        }

        response.status shouldBe HttpStatusCode.OK
        response.body<HomeResponseDto>() shouldBe homeResponseDto
    }
}
