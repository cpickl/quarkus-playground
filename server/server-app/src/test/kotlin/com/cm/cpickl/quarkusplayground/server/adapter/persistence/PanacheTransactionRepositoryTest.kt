package com.cm.cpickl.quarkusplayground.server.adapter.persistence

import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.server.domain.model.NotFoundFail
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import com.cm.cpickl.quarkusplayground.server.domain.model.transactionId
import com.cm.cpickl.quarkusplayground.server.test.rootCause
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.kotest.matchers.types.shouldBeInstanceOf
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.quarkus.test.junit.QuarkusTest
import io.vertx.pgclient.PgException
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import javax.inject.Inject
import javax.persistence.PersistenceException

@QuarkusTest
@Suppress("ClassName")
class PanacheTransactionRepositoryTest {

    private val anyTransactionId = Arb.transactionId().next()

    private val maxTransactionIdLength = 20

    @Inject
    private lateinit var db: DbUtil

    private fun repository() = PanacheTransactionRepository(PanacheTransactionRepositoryPanache())

    @BeforeEach
    fun resetDb() = runUnitBlocking {
        db.reset()
    }

    @Nested
    inner class `When find single`() {
        @Test
        suspend fun `Given none Then fail`() {
            repository().findSingle(anyTransactionId).shouldBeLeft()
                .shouldBeInstanceOf<NotFoundFail>().displayMessage shouldContain anyTransactionId.value
        }

        @Test
        suspend fun `Given one Then return it`() {
            val transactionDbo = Arb.transactionDbo().next()
            db.insert(transactionDbo)

            repository().findSingle(TransactionId(transactionDbo.transactionId))
                .shouldBeRight() shouldBe transactionDbo.toTransaction()
        }
    }

    @Nested
    inner class `When find all`() {
        @Test
        suspend fun `Given none When find all Then return empty`() {
            repository().findAll(PageRequest(skip = 0, take = 1)).shouldBeRight() shouldBe PagedResponseDto(
                items = emptyList(),
                totalItems = 0
            )
        }
    }

    @Test
    fun `When persist with too long ID Then throw proper exception`() = runUnitBlocking {
        val tooLongTransactionId = "x".repeat(maxTransactionIdLength + 1)
        val transaction = Arb.transactionDbo().next().copy(transactionId = tooLongTransactionId)

        val thrown = shouldThrow<PersistenceException> {
            db.insert(transaction)
        }

        thrown.rootCause.shouldNotBeNull().shouldBeInstanceOf<PgException>().message.shouldNotBeNull()
            .also { message ->
                assertSoftly {
                    message shouldContain maxTransactionIdLength.toString()
//                    message shouldContainIgnoringCase "transactionid"
//                    message shouldContain tooLongTransactionId
                }
            }
    }
}
