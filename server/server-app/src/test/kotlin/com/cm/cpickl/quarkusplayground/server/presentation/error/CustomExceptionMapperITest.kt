package com.cm.cpickl.quarkusplayground.server.presentation.error

import com.cm.cpickl.quarkusplayground.api.models.ApiErrorDto
import com.cm.cpickl.quarkusplayground.server.test.TestHttpClient
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotContain
import io.ktor.client.call.body
import io.ktor.http.HttpStatusCode
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.ext.Provider

@QuarkusTest
class CustomExceptionMapperITest {

    @Inject
    private lateinit var httpClient: TestHttpClient

    companion object {
        const val exceptionMessage = "secret"
    }

    @Test
    fun `Given throwing endpoint When request it Then custom mapper handles exception`() = runUnitBlocking {
        val response = httpClient.get("/error")

        response.status shouldBe HttpStatusCode.InternalServerError
        response.body<ApiErrorDto>().message shouldNotContain exceptionMessage
    }

    @Provider
    @Path("/error")
    @Produces(MediaType.APPLICATION_JSON)
    class TestErrorResource {
        @GET
        fun throwException(): Any {
            throw Exception(exceptionMessage)
        }
    }
}
