package com.cm.cpickl.quarkusplayground.server.presentation

import com.cm.cpickl.quarkusplayground.api.test.homeResponseDto
import io.kotest.assertions.json.shouldEqualJson
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test

class HomeApiModelsSerializationTest {

    private val homeResponseDto = Arb.homeResponseDto().next()

    @Test
    fun `When serialize HomeResponseDto Then return JSON`() {
        val json = Json.encodeToString(homeResponseDto)

        json shouldEqualJson """{ 
            "linkThis": "${homeResponseDto.linkThis}",
            "linkTransactions": "${homeResponseDto.linkTransactions}",
            "health": {
              "home": "${homeResponseDto.health.home}",
              "started": "${homeResponseDto.health.started}",
              "ready": "${homeResponseDto.health.ready}",
              "live": "${homeResponseDto.health.live}"
            }
        }"""
    }
}
