package com.cm.cpickl.quarkusplayground.server.adapter.persistence

import io.smallrye.mutiny.coroutines.awaitSuspending
import mu.KotlinLogging.logger
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional

@ApplicationScoped
@Transactional
class DbUtil(
) {

    private val log = logger {}
    private val transactions = PanacheTransactionRepositoryPanache()

    suspend fun reset() {
        log.debug { "reset()" }
        transactions.deleteAll().awaitSuspending()
    }

    suspend fun insert(transactionDbo: TransactionDbo) {
        transactionDbo.persistAndFlush<TransactionDbo>().awaitSuspending()
    }
}
