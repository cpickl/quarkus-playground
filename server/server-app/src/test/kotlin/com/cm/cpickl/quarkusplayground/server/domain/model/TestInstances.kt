package com.cm.cpickl.quarkusplayground.server.domain.model

import com.cm.cpickl.quarkusplayground.commons.domainmodels.Currency
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Money
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string

fun Arb.Companion.transaction() = arbitrary {
    Transaction(
        id = Arb.transactionId().next(),
        money = Arb.money().next(),
    )
}

fun Arb.Companion.transactionId() = arbitrary {
    TransactionId(value = string(minSize = 1, codepoints = Codepoint.alphanumeric()).next())
}

fun Arb.Companion.money() = arbitrary {
    Money(
        currency = currency().next(),
        amount = int(min = 0).next()
    )
}

fun Arb.Companion.currency() = arbitrary {
    Currency(
        code = string(minSize = 3, maxSize = 3, codepoints = Codepoint.alphanumeric()).next(),
        precision = int(min = 0).next()
    )
}
