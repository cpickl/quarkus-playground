package com.cm.cpickl.quarkusplayground.server.presentation.resource

import arrow.core.left
import arrow.core.right
import com.cm.cpickl.quarkusplayground.api.models.ApiErrorDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.api.test.transactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.test.transactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.borealis.server.quarkus.PageRequestBean
import com.cm.cpickl.quarkusplayground.commons.ktor.client.acceptJson
import com.cm.cpickl.quarkusplayground.server.commons.parameter
import com.cm.cpickl.quarkusplayground.server.domain.logic.notFoundFail
import com.cm.cpickl.quarkusplayground.server.presentation.controller.TransactionController
import com.cm.cpickl.quarkusplayground.server.test.TestHttpClient
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.ktor.client.call.body
import io.ktor.http.HttpStatusCode
import io.mockk.coEvery
import io.quarkiverse.test.junit.mockk.InjectMock
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import javax.inject.Inject


@QuarkusTest
// @TestHTTPEndpoint(TransactionResource::class.java) ... for rest-assured
class TransactionResourceTest {

//    @TestHTTPEndpoint(TransactionResource::class)
//    @TestHTTPResource
//    private lateinit var url: URL ... full URL

    @Inject
    private lateinit var httpClient: TestHttpClient

    @InjectMock
    private lateinit var controller: TransactionController

    private val transactionsSummary = Arb.transactionsSummaryResponseDto().next()
    private val transaction = Arb.transactionsDetailDto().next()
    private val unknownId = "unknownTestId"
    private val pageRequestBean = PageRequestBean(skip = 1, take = 2)
    private val notFoundFail = Arb.notFoundFail().next()

    @Test
    fun `Given controller responses When get paged transactions Then return OK and body`() = runUnitBlocking {
        coEvery { controller.findAll(pageRequestBean) } returns transactionsSummary.right()

        val response = httpClient.get("/transactions") {
            acceptJson()
            parameter(pageRequestBean)
        }

        response.status shouldBe HttpStatusCode.OK
        response.body<PagedResponseDto<TransactionsSummaryDto>>() shouldBe transactionsSummary
    }

    @Test
    fun `Given controller responses When get transactions without pagination Then use default pagination`() =
        runUnitBlocking {
            coEvery { controller.findAll(PageRequestBean.default) } returns transactionsSummary.right()

            val response = httpClient.get("/transactions") {
                acceptJson()
            }

            response.status shouldBe HttpStatusCode.OK
            response.body<PagedResponseDto<TransactionsSummaryDto>>() shouldBe transactionsSummary
        }

    @Test
    fun `Given controller returns transaction When find that transaction Then return OK and body`() = runUnitBlocking {
        coEvery { controller.findSingle(transaction.id) } returns transaction.right()

        val response = httpClient.get("/transactions/${transaction.id}") {
            acceptJson()
        }

        response.status shouldBe HttpStatusCode.OK
        response.body<TransactionsDetailDto>() shouldBe transaction
    }

    @Test
    fun `Given controller returns null When find transaction Then return not found`() = runUnitBlocking {
        coEvery { controller.findSingle(unknownId) } returns notFoundFail.left()

        val response = httpClient.get("/transactions/$unknownId") {
            acceptJson()
        }

        response.status shouldBe HttpStatusCode.NotFound
        response.body<ApiErrorDto>() shouldBe ApiErrorDto(notFoundFail.displayMessage)
    }
}
