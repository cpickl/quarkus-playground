package com.cm.cpickl.quarkusplayground.server.test

import io.kotest.common.runBlocking

/** In order to immediately assign and keep the Unit return value. */
fun runUnitBlocking(asyncCode: suspend () -> Unit): Unit {
    runBlocking(asyncCode)
}

/** Kotlin has the same, yet it is internal only :-( */
val Throwable.rootCause: Throwable?
    get() {
        var rootCause: Throwable? = this
        while (rootCause?.cause != null) {
            rootCause = rootCause.cause
        }
        return rootCause
    }
