package com.cm.cpickl.quarkusplayground.server.domain.logic

import com.cm.cpickl.quarkusplayground.server.domain.model.NotFoundFail
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.orNull
import io.kotest.property.arbitrary.string

fun Arb.Companion.notFoundFail() = arbitrary {
    NotFoundFail(
        displayMessage = string().next(),
        internalMessage = string().next(),
        cause = null,
        exception = exception().orNull().next(),
    )
}

// TODO move to common-test
fun Arb.Companion.exception() = arbitrary {
    Exception(string().next())
}
