package com.cm.cpickl.quarkusplayground.server.home

import com.cm.cpickl.quarkusplayground.commons.ktor.client.accept
import com.cm.cpickl.quarkusplayground.server.test.httpClient
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotBeEmpty
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.quarkus.test.junit.QuarkusIntegrationTest
import org.junit.jupiter.api.Test

@Suppress("FunctionName")
@QuarkusIntegrationTest
class HomeResourceITest {

    @Test
    fun `When get home Then return OK and some text`() = runUnitBlocking {
        val response = httpClient.get("/") {
            accept(ContentType.Text.Plain)
        }

        response.status shouldBe HttpStatusCode.OK
        response.bodyAsText().shouldNotBeEmpty()
    }
}
