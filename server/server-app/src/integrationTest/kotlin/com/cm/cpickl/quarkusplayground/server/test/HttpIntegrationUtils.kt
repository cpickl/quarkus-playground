package com.cm.cpickl.quarkusplayground.server.test

// @Inject is not available in integration tests...
val httpClient = TestHttpClient(explicitBaseUrl = System.getProperty("test.url")) // defined by Quarkus
