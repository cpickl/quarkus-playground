package com.cm.cpickl.quarkusplayground.server.transactions

import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.commons.ktor.client.acceptJson
import com.cm.cpickl.quarkusplayground.server.test.httpClient
import com.cm.cpickl.quarkusplayground.server.test.runUnitBlocking
import io.kotest.matchers.shouldBe
import io.ktor.client.call.body
import io.ktor.http.HttpStatusCode
import io.quarkus.test.junit.QuarkusIntegrationTest
import org.junit.jupiter.api.Test

@Suppress("FunctionName")
@QuarkusIntegrationTest
class TransactionResourceITest {

    @Test
    fun `When get transactions Then return OK and a response DTO`() = runUnitBlocking {
        val response = httpClient.get("/transactions") {
            acceptJson()
        }

        response.status shouldBe HttpStatusCode.OK
        response.body<PagedResponseDto<TransactionsSummaryDto>>()
    }
}
