package com.cm.cpickl.quarkusplayground.server.adapter.persistence

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = TransactionDbo.tableName)
class TransactionDbo(

    @Id
    @Column(name = "transactionId", length = 20)
    val transactionId: String, // TODO register custom type? (like with DTO serializers?!)

    val currencyCode: String,
    val amount: Int,

    ) : PanacheEntityBase { // PanacheEntity ... provides default ID: Long

    companion object {
        const val tableName = "transaction"
    }

    /** Manual copy, as data class would introduce JPA disrecommended equals/hashCode. */
    fun copy(
        transactionId: String = this.transactionId,
        currencyCode: String = this.currencyCode,
        amount: Int = this.amount,
    ) = TransactionDbo(
        transactionId = transactionId,
        currencyCode = currencyCode,
        amount = amount,
    )

}
