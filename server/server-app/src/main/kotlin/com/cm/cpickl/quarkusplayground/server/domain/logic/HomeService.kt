package com.cm.cpickl.quarkusplayground.server.domain.logic

import javax.enterprise.context.ApplicationScoped

interface HomeService {
    fun greeting(): String
}

@ApplicationScoped
class HomeServiceImpl : HomeService {
    override fun greeting() = "Hello Quarkus Playground!"
}
