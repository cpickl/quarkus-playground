package com.cm.cpickl.quarkusplayground.server.quarkus

import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter
import javax.ws.rs.ext.Provider

@Provider
class CorsInterceptor : ContainerResponseFilter {
    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        with(responseContext.headers) {
            add("access-control-allow-origin", "*")
            add("access-control-allow-methods", "GET, POST, PUT, DELETE, OPTIONS")
            add("access-control-allow-headers", "*")
            add("access-control-expose-headers", "*")
        }
    }
}
