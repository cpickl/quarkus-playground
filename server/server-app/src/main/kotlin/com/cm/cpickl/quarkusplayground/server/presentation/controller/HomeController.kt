package com.cm.cpickl.quarkusplayground.server.presentation.controller

import com.cm.cpickl.quarkusplayground.api.models.HealthResponseDto
import com.cm.cpickl.quarkusplayground.api.models.HomeResponseDto
import com.cm.cpickl.quarkusplayground.server.LinkGenerator
import com.cm.cpickl.quarkusplayground.server.domain.logic.HomeService
import javax.enterprise.context.ApplicationScoped

interface HomeController {
    fun greeting(): String
    fun home(): HomeResponseDto
}

@ApplicationScoped
class HomeControllerImpl(
    private val service: HomeService,
    private val linkGenerator: LinkGenerator,
) : HomeController {

    override fun greeting() = service.greeting()

    override fun home() = HomeResponseDto(
        linkThis = linkGenerator.home(),
        linkTransactions = linkGenerator.transactions.summary(),
        linkOpenApi = linkGenerator.openApi,
        linkSwaggerUi = linkGenerator.swaggerUi,
        health = HealthResponseDto(
            home = linkGenerator.health.home(),
            started = linkGenerator.health.started(),
            ready = linkGenerator.health.ready(),
            live = linkGenerator.health.live(),
        ),
    )
}
