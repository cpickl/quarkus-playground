package com.cm.cpickl.quarkusplayground.server.adapter

import com.cm.cpickl.quarkusplayground.server.Config
import com.cm.cpickl.quarkusplayground.server.adapter.persistence.PanacheTransactionRepository
import com.cm.cpickl.quarkusplayground.server.adapter.persistence.PanacheTransactionRepositoryPanache
import com.cm.cpickl.quarkusplayground.server.domain.port.TransactionRepository
import io.vertx.mutiny.pgclient.PgPool
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.Produces

// https://docs.jboss.org/weld/reference/latest/en-US/html/producermethods.html
@ApplicationScoped
class TransactionRepositoryProvider(
    private val config: Config
) {
    @Produces
    @ApplicationScoped
    fun transactionRepository(client: PgPool): TransactionRepository =
        if (config.useInMemoryTransactionService) {
            InMemoryTransactionRepository()
        } else {
            PanacheTransactionRepository(PanacheTransactionRepositoryPanache())
        }
}
