package com.cm.cpickl.quarkusplayground.server.adapter.persistence

import arrow.core.Either
import arrow.core.continuations.either
import arrow.core.left
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Currency
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Money
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.NotFoundFail
import com.cm.cpickl.quarkusplayground.server.domain.model.PersistenceFail
import com.cm.cpickl.quarkusplayground.server.domain.model.Transaction
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import com.cm.cpickl.quarkusplayground.server.domain.port.TransactionRepository
import io.quarkus.hibernate.reactive.panache.kotlin.PanacheRepository
import io.quarkus.panache.common.Sort
import io.smallrye.mutiny.coroutines.awaitSuspending

class PanacheTransactionRepository(
    private val panache: PanacheRepository<TransactionDbo>
) : TransactionRepository {

    override suspend fun findAll(pageRequest: PageRequest): Either<Fail, PagedResponseDto<Transaction>> = either {
        PagedResponseDto(
            items = panache.findAll(Sort.ascending("transactionId"))
                .range(pageRequest.skip, pageRequest.skip + pageRequest.take)
                .list().awaitSuspending().map { it.toTransaction().bind() },
            totalItems = panache.count().awaitSuspending()
        )
    }

    override suspend fun findSingle(id: TransactionId): Either<Fail, Transaction> = either {
        val awaitSuspending = panache.find("transactionId = ?1", id.value).firstResult().awaitSuspending()
        if (awaitSuspending == null) {
            NotFoundFail("Transaction not found by ID: $id").left()
        }
        awaitSuspending!!.toTransaction().bind()
    }
}

class PanacheTransactionRepositoryPanache : PanacheRepository<TransactionDbo>

suspend fun TransactionDbo.toTransaction(): Either<Fail, Transaction> = either {
    val currency = Currency.byCurrencyCode(currencyCode)
        .mapLeft { PersistenceFail("Corrupt database data for transaction with ID: $transactionId (${it.message})") }
        .bind()
    Transaction(
        id = TransactionId(value = transactionId),
        money = Money(currency = currency, amount = amount),
    )
}
