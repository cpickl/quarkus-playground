package com.cm.cpickl.quarkusplayground.server

import io.quarkus.runtime.Quarkus
import io.quarkus.runtime.QuarkusApplication
import mu.KotlinLogging.logger
import org.eclipse.microprofile.config.inject.ConfigProperty

object QuarkusPlaygroundApp {

    private val log = logger {}

    @JvmStatic
    fun main(args: Array<String>) {
        log.info { "Starting server ..." }
        Quarkus.run(QuarkusApp::class.java, *args)
    }
}

class QuarkusApp(
    @ConfigProperty(name = "quarkus.http.port")
    private val port: Int
) : QuarkusApplication {

    private val log = logger {}

    override fun run(vararg args: String?): Int {
        log.info { "Waiting on port $port..." }
        Quarkus.waitForExit()
        return 0
    }
}
