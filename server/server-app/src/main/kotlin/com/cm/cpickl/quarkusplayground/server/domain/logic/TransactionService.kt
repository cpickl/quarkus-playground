package com.cm.cpickl.quarkusplayground.server.domain.logic

import arrow.core.Either
import arrow.core.continuations.either
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.Transaction
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import com.cm.cpickl.quarkusplayground.server.domain.port.TransactionRepository
import javax.enterprise.context.ApplicationScoped

interface TransactionService {
    suspend fun findAll(pageRequest: PageRequest): Either<Fail, PagedResponseDto<Transaction>>
    suspend fun findSingle(id: TransactionId): Either<Fail, Transaction>
}

@ApplicationScoped
class TransactionServiceImpl(
    private val transactionRepository: TransactionRepository,
) : TransactionService {

    override suspend fun findAll(pageRequest: PageRequest): Either<Fail, PagedResponseDto<Transaction>> = either {
        transactionRepository.findAll(pageRequest).bind()
    }

    override suspend fun findSingle(id: TransactionId): Either<Fail, Transaction> = either {
        transactionRepository.findSingle(id).bind()
    }
}
