package com.cm.cpickl.quarkusplayground.server.adapter

import arrow.core.Either
import arrow.core.continuations.either
import arrow.core.left
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Currency
import com.cm.cpickl.quarkusplayground.commons.domainmodels.Money
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.NotFoundFail
import com.cm.cpickl.quarkusplayground.server.domain.model.Transaction
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import com.cm.cpickl.quarkusplayground.server.domain.port.TransactionRepository

class InMemoryTransactionRepository : TransactionRepository {

    private val transactions = List(42) {
        Transaction(
            id = TransactionId("ID${it + 1}"),
            money = Money(Currency.euro, it * 100)
        )
    }

    override suspend fun findAll(pageRequest: PageRequest): Either<Fail, PagedResponseDto<Transaction>> = either {
        PagedResponseDto(
            items = transactions.drop(pageRequest.skip).take(pageRequest.take),
            totalItems = transactions.size.toLong()
        )
    }

    override suspend fun findSingle(id: TransactionId): Either<Fail, Transaction> = either {
        val found = transactions.firstOrNull { it.id == id }
        if (found == null) {
            NotFoundFail("Transaction not found: $id").left()
        }
        found!!
    }
}
