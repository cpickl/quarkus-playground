package com.cm.cpickl.quarkusplayground.server.domain.model

sealed interface Fail {
    val displayMessage: String
    val internalMessage: String
    val exception: Exception?
    val cause: Fail?
}

// @formatter:off
data class NotFoundFail   (override val displayMessage: String, override val internalMessage: String = displayMessage, override val exception: Exception? = null, override val cause: Fail? = null) : Fail
data class UnknownFail    (override val displayMessage: String, override val internalMessage: String = displayMessage, override val exception: Exception? = null, override val cause: Fail? = null) : Fail
data class PersistenceFail(override val displayMessage: String, override val internalMessage: String = displayMessage, override val exception: Exception? = null, override val cause: Fail? = null) : Fail
//@formatter:on
