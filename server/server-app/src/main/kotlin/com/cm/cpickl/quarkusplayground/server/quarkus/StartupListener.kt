package com.cm.cpickl.quarkusplayground.server.quarkus

import io.quarkus.runtime.StartupEvent
import mu.KotlinLogging.logger
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.Observes

@ApplicationScoped
class StartupListener(
    @ConfigProperty(name = "quarkus.profile")
    private val profile: String
) {
    private val log = logger {}
    fun onStart(@Suppress("unused") @Observes event: StartupEvent) {
        log.info { "Server started up (profile=$profile)." }
    }
}