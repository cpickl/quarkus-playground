package com.cm.cpickl.quarkusplayground.server.domain.model

import com.cm.cpickl.quarkusplayground.commons.domainmodels.Money

data class Transaction(
    val id: TransactionId,
    val money: Money,
)

@JvmInline
value class TransactionId(val value: String) {
    override fun toString() = value
}
