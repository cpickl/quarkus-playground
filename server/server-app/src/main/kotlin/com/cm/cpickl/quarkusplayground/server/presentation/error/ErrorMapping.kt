package com.cm.cpickl.quarkusplayground.server.presentation.error

import com.cm.cpickl.quarkusplayground.api.models.ApiErrorDto
import mu.KotlinLogging.logger
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class CustomExceptionMapper : ExceptionMapper<Exception> {

    private val log = logger {}

    override fun toResponse(exception: Exception): Response {
        log.error(exception) { "Unhandled exception was caught by representation error mapper!" }
        return Response.status(500).entity(ApiErrorDto("Internal server error (check logs for details).")).build()
    }
}