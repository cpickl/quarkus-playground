package com.cm.cpickl.quarkusplayground.server.domain.port

import arrow.core.Either
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PagedResponseDto
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.Transaction
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId

interface TransactionRepository {

    suspend fun findAll(pageRequest: PageRequest): Either<Fail, PagedResponseDto<Transaction>>

    suspend fun findSingle(id: TransactionId): Either<Fail, Transaction>

}
