package com.cm.cpickl.quarkusplayground.server.presentation.error

import arrow.core.Either
import com.cm.cpickl.quarkusplayground.api.models.ApiErrorDto
import com.cm.cpickl.quarkusplayground.commons.apimodels.Dto
import com.cm.cpickl.quarkusplayground.server.commons.LogLevel
import com.cm.cpickl.quarkusplayground.server.commons.logByLevel
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.NotFoundFail
import com.cm.cpickl.quarkusplayground.server.domain.model.PersistenceFail
import com.cm.cpickl.quarkusplayground.server.domain.model.UnknownFail
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging.logger
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.Response

@ApplicationScoped
class EitherResponseMapper {

    private val log = logger {}

    final inline fun <reified DTO : Dto> map(either: Either<Fail, DTO>): Response =
        either.fold(
            ifRight = { result ->
                Response
                    .status(200)
                    .header("Content-Type", "application/json")
                    // there was an issue with serializing PagedResponse (only within quarkus, kotlinx-serialization itself was dealing with it ok)
                    .entity(Json.encodeToString(result))
                    .build()
            },
            ifLeft = { fail ->
                logFail(fail)
                when (fail) {
                    is NotFoundFail -> Response.status(404).entity(ApiErrorDto(fail.displayMessage)).build()
                    is PersistenceFail,
                    is UnknownFail -> Response.status(500).entity(ApiErrorDto("Unknown server error")).build()
                }
            }
        )

    private val Fail.logLevel
        get() = when (this) {
            is NotFoundFail -> LogLevel.Warn
            is PersistenceFail,
            is UnknownFail -> LogLevel.Error
        }

    fun logFail(fail: Fail, isCause: Boolean = false) {
        log.logByLevel(fail.logLevel, fail.exception) {
            "${if (isCause) "Caused by" else "Failed"}: ${fail.displayMessage} / ${fail.internalMessage}"
        }
        fail.cause?.also { cause ->
            logFail(cause, isCause = true)
        }
    }
}
