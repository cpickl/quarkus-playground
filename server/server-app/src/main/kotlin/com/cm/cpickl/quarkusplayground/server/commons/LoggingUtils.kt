package com.cm.cpickl.quarkusplayground.server.commons

import mu.KLogger

fun KLogger.logByLevel(logLevel: LogLevel, exception: Exception?, messageProvider: () -> String) {
    when (logLevel) {
        LogLevel.Error -> error(exception, messageProvider)
        LogLevel.Warn -> warn(exception, messageProvider)
    }
}

enum class LogLevel {
    Error,
    Warn,
}
