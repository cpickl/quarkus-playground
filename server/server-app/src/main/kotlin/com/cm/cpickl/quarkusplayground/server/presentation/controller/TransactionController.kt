package com.cm.cpickl.quarkusplayground.server.presentation.controller

import arrow.core.Either
import arrow.core.continuations.either
import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.server.quarkus.PageRequestBean
import com.cm.cpickl.quarkusplayground.commons.modeltransformers.toMoneyDto
import com.cm.cpickl.quarkusplayground.server.LinkGenerator
import com.cm.cpickl.quarkusplayground.server.domain.logic.TransactionService
import com.cm.cpickl.quarkusplayground.server.domain.model.Fail
import com.cm.cpickl.quarkusplayground.server.domain.model.Transaction
import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import javax.enterprise.context.ApplicationScoped

interface TransactionController {
    suspend fun findAll(pageRequestBean: PageRequestBean): Either<Fail, TransactionsSummaryResponseDto>
    suspend fun findSingle(id: String): Either<Fail, TransactionsDetailDto>
}

@ApplicationScoped
class TransactionControllerImpl(
    private val service: TransactionService,
    private val linkGenerator: LinkGenerator,
) : TransactionController {

    override suspend fun findAll(pageRequestBean: PageRequestBean): Either<Fail, TransactionsSummaryResponseDto> =
        either {
            TransactionsSummaryResponseDto(
                service.findAll(pageRequestBean.toPageRequest()).bind().map { it.toTransactionsSummaryDto() })
        }

    override suspend fun findSingle(id: String) = either {
        service.findSingle(TransactionId(id)).bind().toTransactionsDetailDto()
    }

    private fun Transaction.toTransactionsSummaryDto() = TransactionsSummaryDto(
        id = id.value,
        money = money.toMoneyDto(),
        linkDetail = linkGenerator.transactions.detail(id),
    )

    private fun Transaction.toTransactionsDetailDto() = TransactionsDetailDto(
        id = id.value,
        money = money.toMoneyDto(),
    )
}
