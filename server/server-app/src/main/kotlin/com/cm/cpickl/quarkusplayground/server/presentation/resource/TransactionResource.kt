package com.cm.cpickl.quarkusplayground.server.presentation.resource

import com.cm.cpickl.quarkusplayground.api.models.ApiErrorDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsDetailDto
import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryResponseDto
import com.cm.cpickl.quarkusplayground.borealis.server.quarkus.PageRequestBean
import com.cm.cpickl.quarkusplayground.server.presentation.controller.TransactionController
import com.cm.cpickl.quarkusplayground.server.presentation.error.EitherResponseMapper
import mu.KotlinLogging.logger
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.info.Info
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import javax.ws.rs.BeanParam
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path(TransactionResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@OpenAPIDefinition(
    info = Info(
        title = "Transactions",
        description = "CRUD operations for transactions.",
        version = "1",
    )
)
class TransactionResource(
    private val controller: TransactionController,
    private val eitherMapper: EitherResponseMapper
) {
    companion object {
        const val PATH = "/transactions"
    }

    private val log = logger {}

    @GET
    @Operation(summary = "Get a page of transaction summaries")
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "200",
                description = "A page of transaction summaries.",
                content = [
                    Content(
                        mediaType = MediaType.APPLICATION_JSON,
                        schema = Schema(
                            type = SchemaType.OBJECT,
                            implementation = TransactionsSummaryResponseDto::class
                        )
                    )
                ]
            )
        ]
    )
    suspend fun summary(@BeanParam pageRequestBean: PageRequestBean): Response {
        log.info { "GET $PATH ($pageRequestBean)" }
        return eitherMapper.map(controller.findAll(pageRequestBean))
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "Find details of a transaction by its ID.")
    @Parameter(
        name = "id",
        description = "The identifier of a transaction.",
        example = "ID42",
        schema = Schema(type = SchemaType.STRING) // format = "uuid"
    )
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "200",
                description = "Transaction details.",
                content = [
                    Content(
                        mediaType = MediaType.APPLICATION_JSON,
                        schema = Schema(
                            type = SchemaType.OBJECT,
                            implementation = TransactionsDetailDto::class
                        )
                    )
                ]
            ),
            APIResponse(
                responseCode = "404",
                description = "Requested transaction was not found.",
                content = [
                    Content(
                        mediaType = MediaType.APPLICATION_JSON,
                        schema = Schema(
                            type = SchemaType.OBJECT,
                            implementation = ApiErrorDto::class
                        )
                    )
                ]
            )
        ]
    )
    // TODO use `@RestPath id: String` instead?
    suspend fun detail(@PathParam("id") id: String): Response {
        log.info { "GET $PATH/$id" }
        return eitherMapper.map(controller.findSingle(id))
    }
}
