package com.cm.cpickl.quarkusplayground.server

import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class Config {
    val useInMemoryTransactionService = false
}
