package com.cm.cpickl.quarkusplayground.server.presentation.resource

import com.cm.cpickl.quarkusplayground.api.models.HomeResponseDto
import com.cm.cpickl.quarkusplayground.server.presentation.controller.HomeController
import mu.KotlinLogging.logger
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/")
class HomeResource(
    private val controller: HomeController
) {

    private val log = logger {}

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    fun homeText(): String {
        log.info { "GET / (text)" }
        return controller.greeting()
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun homeJson(): HomeResponseDto {
        log.info { "GET / (json)" }
        return controller.home()
    }
}
