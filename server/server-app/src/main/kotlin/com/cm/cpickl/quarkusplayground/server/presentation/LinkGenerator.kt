package com.cm.cpickl.quarkusplayground.server

import com.cm.cpickl.quarkusplayground.server.domain.model.TransactionId
import com.cm.cpickl.quarkusplayground.server.presentation.resource.TransactionResource
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.Context
import javax.ws.rs.core.UriInfo

interface LinkGenerator {
    fun home(): String
    val transactions: TransactionsLinkGenerator
    val openApi: String
    val swaggerUi: String
    val health: HealthLinkGenerator
}

interface TransactionsLinkGenerator {
    fun summary(): String
    fun detail(id: TransactionId): String
}

interface HealthLinkGenerator {
    fun home(): String
    fun ready(): String
    fun started(): String
    fun live(): String
}

@ApplicationScoped
class LinkGeneratorImpl(
    @ConfigProperty(name = "quarkus.http.port")
    private val port: Int,
    @Context
    private val uriInfo: UriInfo,
) : LinkGenerator {

    private val protocalAndHost = "http://localhost" // TODO figure out at runtime base URL
    private val baseUrl = "$protocalAndHost:$port"
    override val openApi = "$baseUrl/q/openapi"
    override val swaggerUi = "$baseUrl/q/swagger-ui"
    override fun home() = baseUrl

    override val transactions = object : TransactionsLinkGenerator {
        override fun summary() = "$baseUrl${TransactionResource.PATH}"
        override fun detail(id: TransactionId) = "$baseUrl${TransactionResource.PATH}/$id"
    }

    override val health = object : HealthLinkGenerator {
        override fun home() = "$baseUrl/q/health"
        override fun ready() = "$baseUrl/q/ready"
        override fun started() = "$baseUrl/q/started"
        override fun live() = "$baseUrl/q/live"
    }
}
