import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    mavenCentral()
}

plugins {
    kotlin("jvm") version "1.7.21"
    kotlin("plugin.serialization") version "1.7.21"
    kotlin("plugin.allopen") version "1.7.21"
    id("io.quarkus") version "2.14.1.Final"
    id("com.github.ben-manes.versions") version "0.43.0"
//    kotlin("allopen") version "1.7.21"
    kotlin("plugin.jpa") version "1.7.21" // to generate default constructor for hibernate
}

dependencies {
    implementation(project(":server:server-api:server-api-models"))
    implementation(project(":commons:common-domain-models"))
    implementation(project(":commons:common-model-transformers"))
    implementation(project(":borealis:borealis-server:borealis-server-quarkus"))

    implementation(kotlin("stdlib-jdk8"))
    implementation("io.arrow-kt:arrow-core:1.1.2")
    implementation("io.github.microutils:kotlin-logging:3.0.4")

    // QUARKUS
    // =================================================================================================================
    implementation(enforcedPlatform("io.quarkus.platform:quarkus-bom:2.14.1.Final"))
    implementation("io.quarkus:quarkus-kotlin")
    implementation("io.quarkus:quarkus-arc") // provides CDI
    implementation("io.quarkus:quarkus-config-yaml")
    // see: https://developers.redhat.com/articles/2022/02/03/build-rest-api-ground-quarkus-20#observability
    implementation("io.smallrye.reactive:mutiny-kotlin:1.7.0") // https://smallrye.io/smallrye-mutiny/1.7.0/guides/kotlin/
    if (System.getProperty("os.name").contains("mac")) {
        println("Adding macOS DNS resolver.")
        // TODO fix warning with warning on macOS regarding DNS resolution ---- doesn't work, still warning in log at startup?!
        runtimeOnly("io.netty:netty-resolver-dns-native-macos:4.1.85.Final")
    }

    // WEB
    // =================================================================================================================
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
    implementation("io.quarkus:quarkus-resteasy-reactive")
//    implementation("io.quarkus:quarkus-resteasy") ... already provided by reactive
    implementation("io.quarkus:quarkus-resteasy-reactive-kotlin-serialization")
    implementation("io.quarkus:quarkus-smallrye-health")
    implementation("io.quarkus:quarkus-smallrye-openapi")

    // PERSISTENCE
    // =================================================================================================================
    // https://quarkus.io/guides/hibernate-orm#setting-up-and-configuring-hibernate-orm
    implementation("io.quarkus:quarkus-hibernate-reactive-panache-kotlin") // this is super new, quarkus v2.14.0 and up
    implementation("io.quarkus:quarkus-reactive-pg-client")

    // TEST
    // =================================================================================================================
    testImplementation(project(":server:server-api:server-api-sdk"))
    testImplementation(project(":server:server-api:server-api-test-models"))
    testImplementation(project(":borealis:borealis-server:borealis-server-ktor"))
    testImplementation(project(":commons:common-ktor:common-ktor-client"))
    testImplementation("io.quarkus:quarkus-junit5")
    testImplementation("io.ktor:ktor-client-core:2.1.3")
    testImplementation("io.ktor:ktor-client-cio:2.1.3")
    testImplementation("io.ktor:ktor-client-content-negotiation:2.1.3")
    testImplementation("io.ktor:ktor-serialization-kotlinx-json:2.1.3")
    testImplementation("io.kotest:kotest-assertions-core:5.5.4")
    testImplementation("io.kotest:kotest-assertions-json:5.5.4")
    testImplementation("io.kotest:kotest-property:5.5.4")
    testImplementation("io.kotest.extensions:kotest-assertions-arrow:1.2.5")
    testImplementation("io.quarkiverse.mockk:quarkus-junit5-mockk:1.1.1")
}

// TEST: systemProperty "java.util.logging.manager", "org.jboss.logmanager.LogManager"

tasks.withType<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask> {
    val rejectPatterns =
        listOf("Alpha1", "alpha", "beta", "EAP", "RC", "rc-1", "m", "CR1").map { it.toLowerCase() }
    rejectVersionIf {
        val version = candidate.version.toLowerCase()
        rejectPatterns.any { version.endsWith(it) }
    }
}

//tasks.quarkusDev {
//}

//tasks.test {
// custom test configuration profile:
// systemProperty("quarkus.test.profile", "foo")
//}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        freeCompilerArgs = listOf(
            "-Xjsr305=strict"
        )
        jvmTarget = JavaVersion.VERSION_11.toString()
        javaParameters = true // quarkus requires (recommends?) this => https://quarkus.io/guides/kotlin#important-gradle-configuration-points
    }
}

allOpen {
    annotation("javax.ws.rs.Path")
    annotation("javax.enterprise.context.ApplicationScoped")
    annotation("io.quarkus.test.junit.QuarkusTest")
    annotation("javax.persistence.Entity")
}
