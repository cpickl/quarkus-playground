repositories {
    mavenCentral()
}

plugins {
    kotlin("jvm") version "1.7.21" apply false
    kotlin("multiplatform") version "1.7.21" apply false
    kotlin("plugin.serialization") version "1.7.21" apply false
    id("io.kotest.multiplatform") version "5.5.4" apply false
    id("com.github.ben-manes.versions") version "0.43.0" apply false
}

allprojects {
    group = "com.cm.cpickl.quarkusplayground"
    version = "1.0-SNAPSHOT"
}

subprojects {
    apply<MavenPublishPlugin>()
    apply(plugin = "com.github.ben-manes.versions")
}
