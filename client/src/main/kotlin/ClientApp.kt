import com.cm.cpickl.quarkusplayground.server.api.sdk.ServerApiKtorClient
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import react.create
import react.dom.client.createRoot

fun main() {
    val container = document.getElementById("rootContainer")
        ?: error("Could not find HTML element with ID 'rootContainer'!")
    document.body?.appendChild(container) ?: error("Document body is not available!")

    val serverApi = ServerApiKtorClient("http://localhost:9900")
    createRoot(container).render(rootView(serverApi).create())
}

val mainCoroutineScope = MainScope()
