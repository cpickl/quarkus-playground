import com.cm.cpickl.quarkusplayground.api.models.TransactionsSummaryDto
import com.cm.cpickl.quarkusplayground.borealis.apimodels.PageRequest
import com.cm.cpickl.quarkusplayground.server.api.sdk.ServerApi
import kotlinx.browser.window
import kotlinx.coroutines.launch
import react.FC
import react.Props
import react.dom.html.ReactHTML
import react.useEffectOnce
import react.useState

fun rootView(serverApi: ServerApi) = FC<Props> { _ ->
    var greeting by useState("Loading ...")
    var transactions by useState(emptyList<TransactionsSummaryDto>())

    useEffectOnce {
        mainCoroutineScope.launch {
            greeting = serverApi.homeGreetingString()
            transactions = serverApi.findAllTransactions(PageRequest.default).items
        }
    }

    ReactHTML.h1 {
        +greeting
    }
    ReactHTML.ul {
        transactions.forEach { tx ->
            ReactHTML.li {
                ReactHTML.button {
                    +tx.id
                    onClick = {
                        window.alert("fetching: ${tx.linkDetail}")
                    }
                }
            }
        }
    }
}
