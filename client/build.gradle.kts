plugins {
    kotlin("js") version "1.7.21"
    kotlin("plugin.serialization") version "1.7.21"
    id("io.kotest.multiplatform") version "5.5.1"
    id("com.github.ben-manes.versions") version "0.42.0"
}

dependencies {
    implementation("com.cm.cpickl.quarkusplayground.server.api:server-api-sdk:1.0-SNAPSHOT")

    // TODO should be there transitively via server-api-sdk?!
    implementation("com.cm.cpickl.quarkusplayground.borealis:borealis-api-models:1.0-SNAPSHOT")
    implementation("com.cm.cpickl.quarkusplayground.commons:common-api-models:1.0-SNAPSHOT")

    implementation("org.jetbrains.kotlin-wrappers:kotlin-react:18.0.0-pre.332-kotlin-1.6.21")
    implementation("org.jetbrains.kotlin-wrappers:kotlin-react-dom:18.0.0-pre.332-kotlin-1.6.21")
    implementation("org.jetbrains.kotlin-wrappers:kotlin-emotion:11.9.0-pre.332-kotlin-1.6.21")
    implementation("io.ktor:ktor-client-core:2.1.3")
    implementation("io.ktor:ktor-client-js:2.1.3")
    implementation("io.ktor:ktor-client-logging:2.1.3")
    implementation("io.ktor:ktor-client-content-negotiation:2.1.3")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.1.3")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
    implementation("io.github.microutils:kotlin-logging:2.1.23")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.6.4")

    testImplementation("io.kotest:kotest-framework-engine-js:5.5.1")
    testImplementation("io.kotest:kotest-framework-api-js:5.5.1")
    testImplementation("io.kotest:kotest-assertions-core-js:5.5.1")
    testImplementation("io.kotest:kotest-property-js:5.5.1")
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {
            commonWebpackConfig {
                cssSupport.enabled = true
            }
            testTask {
                debug = true
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
    }
}
