# quarkus-playground

Just to learn and showcase Quarkus features.

## Conclusion

* Quarkus is like Apple: As long as you use everything from the same provider, it's great. Once you want to use something from a different (unsupported) vendor, it is almost impossible to wire it in.
* Quarkus is like Maven: As long as you do it their way, the standardized way, it's great. Once you divert even slightly from the given path, you are in hell's kitchen.

### Details

Pros:

* It is great for rapid development, just like Spring-Boot. Once you know it, it's super quick.
* Adhereing industry standards (JAX-RS, JPA, CDI) and also de-facto standards (JUnit, Hibernate).
* Smooth integration of kotlinx serialization.
* Possibility to run from main method.
* Well documented.

Cons:

* It is highly biased; predefining all kind of technology choices.
* It is a Java focused framework, not a Kotlin one. E.g. coroutines. Although reactive seems a solution (Either, Ktor),
  integration with Kotlin is rather poor.
* Only some DBMSes are supported, all the others are rather difficult to wire in (e.g. H2 and reactive).
* Enforcing JUnit, not being able to plugin TestNG or kotest.
* No (real) support for coroutines (no arrow).
* Annotation-based is not as good as a code-based approach (flexibility).
  * Tests can't easily define their own setup, as things are annotations, instead of code.
  * Can't isolate parts of the application (presentation/domain/persistence), due to global component scan.
* It is like JPA/Hibernate itself: Very high abstraction level; if things go wrong, in-depth knowledge of the tool
  itself, and also the way it was integrated is necessary (and usually not available). Compared to code-only, where
  everything is visible, without "black magic".
* Configuration via text files, instead of code. (YAML is a bit better at least)
* "Sometimes" fails due to JVM 8 vs 11 error (inlining code)
* DTO can't be OpenAPI annotated as they are not in an MPP library

## Roadmap

### Focus

* Focus on borealis!
  * Along with shared-frontend components
* Quarkus specific features
* Quarkus and kotlin idiomatic integration

### Todo

* test/dev VS prod DB configuration
* Dockerize
* buildSrc version mgmt
* full CRUD (request/response DTOs)
* bean validation
* Support sorting
* [DevUI](https://quarkus.io/guides/dev-ui) with custom extension
* [Continuous Testing](https://quarkus.io/guides/continuous-testing)
* GraphQL
* Scheduler
* Caching
* Add api-model and api-sdk modules
* Web-client (refactor submodules/reactor)
* Dynamic, programmatic beans via @Provides
* Websockets
* Datetime + custom serializer
* Use H2 for dev/test and PostGres for real
* Liquibase (automatic DiffXML generation)
* Full fledged integration tests (enter via HTTP, prepare via DB); facing the transactional issue
* Packaging (fast-jar, uber-jar)
* Native?!

### Done

* Models using kotlinx-serialization
* Logging with mu-kotlin
* Support of pagination
* Using mockk (along JUnit)
* Integration tests for resource (Kotest assertions, Ktor HTTP client)
* Persistance via JPA/Hibernate/Postgres
* Reactive Resources & Database
* Smallrye health endpoint
* Arrow's either
* OpenAPI/Swagger

## CornerFacts

* run integration tests: `./gradlew quarkusIntTest`
* maven by default, but [supports gradle](https://quarkus.io/guides/gradle-tooling)
  * e.g. add an extension: `./gradlew addExtension --extensions='smallrye-*'`
* use the [`quarkus` CLI tool](https://quarkus.io/guides/cli-tooling):
  * create new service
    skeleton: `quarkus create app my-groupId:my-artifactId --gradle` ([help](https://quarkus.io/guides/gradle-tooling))
  * adding extensions: `/gradlew addExtension --extensions="jdbc,agroal,non-exist-ent"`
  * run the server: `./gradlew --console=plain quarkusDev`
    * in DEV mode, things are different, e.g.: [continuous testing](https://quarkus.io/guides/continuous-testing)
* live coding? continuous dev should allow that...
* [remote coding](https://quarkus.io/guides/gradle-tooling#remote-development-mode)
  * execute: `./gradlew quarkusRemoteDev -Dquarkus.live-reload.url=http://my-remote-host:8080`
* [work offline](https://quarkus.io/guides/gradle-tooling#downloading-dependencies-for-offline-development-and-testing): `./gradlew quarkusGoOffline`

## Downsides

* works nicely with maven (documentation); yet gradle half-way supported
  * continuous testing fails by default ("failed to create compiler" because no maven model)
* multi-modules will fail for quarkus-gradle plugin:
  * got an internal gradle error ([stackoverflow, finalizergroup](https://github.com/gradle/gradle/issues/21997));
    migrating to 1.5 solved it
  * after downgrading, got NPE, as [java is missing](https://github.com/quarkusio/quarkus/issues/21106) for MPP kotlin

### MPP

* when MPP requires ktor-client, fails build with kotlinx-coroutines not found
  * solution: add core-js and core-jvm for each dependency scope

### Quarkus+Kotlin

* as JUnit is THE testing framework for Quarkus, it does not suspend Kotlin's coroutines, thus no suspended test
  function.
  * workaround: use `runBlocking` for each suspended test...

## Resources

* docs: https://quarkus.io/guides/
* sources: https://github.com/quarkusio
* workshop: https://quarkus.io/quarkus-workshops/super-heroes/
* start io: https://code.quarkus.io/