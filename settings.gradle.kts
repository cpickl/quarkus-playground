@file:Suppress("UnstableApiUsage")

rootProject.name = "quarkus-playground"

pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        mavenCentral()
    }
}

include(":server")
include(":server:server-app")
include(":server:server-api")
include(":server:server-api:server-api-models")
include(":server:server-api:server-api-sdk")
include(":server:server-api:server-api-test-models")
include(":commons")
include(":commons:common-lang")
include(":commons:common-api-models")
include(":commons:common-test-api-models")
include(":commons:common-domain-models")
include(":commons:common-test-domain-models")
include(":commons:common-model-transformers")
include(":commons:common-ktor:common-ktor-core")
include(":commons:common-ktor:common-ktor-client")
include(":commons:common-ktor:common-ktor-mock")
include(":borealis")
include(":borealis:borealis-api-models")
include(":borealis:borealis-server")
include(":borealis:borealis-client")
include(":borealis:borealis-client:borealis-client-ktor")
include(":borealis:borealis-server:borealis-server-quarkus")
include(":borealis:borealis-server:borealis-server-ktor")
// include(":client") ... no, standalone!
